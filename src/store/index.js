import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
import VueAxios from "vue-axios";
import Prismic from "prismic-javascript";
import Mixpanel from 'mixpanel-browser';

Vue.use(Vuex);
Vue.use(VueAxios, axios);
Vue.use(Prismic);
Vue.use(Mixpanel);

const store = new Vuex.Store({
  state: {
    step: 0,
    params: {},
    data: null,
    answers: [],
    ip: null,
    errors: []
  },

  actions: {
    loadData({ commit }) {
      // load data from prismic API for specific id
      if (this.state.params.id !== null) {
        // get refs from API
        axios({
          method: "GET",
          url: "https://qunofunnels.cdn.prismic.io/api/v2"
        })
          .then(response => {
            // get master ref
            var ref = response.data.refs.filter(ref => {
              return ref.id == "master";
            })[0].ref;

            // config for fetching json
            const getConfig = () => {
              const config = {
                method: "GET",
                url: `https://qunofunnels.prismic.io/graphql?query=%7B%0A%20%20allFunnels%28id%3A%20%22${this.state.params.id.replace(
                  "#",
                  ""
                )}%22%29%20%7B%0A%20%20%20%20edges%20%7B%0A%20%20%20%20%20%20node%20%7B%0A%20%20%20%20%20%20%20%20funnel_name%2C%0A%20%20%20%20%20%20%20%20funnel_id%2C%0A%20%20%20%20%20%20%20%20continue_button_label%2C%0A%20%20%20%20%20%20%20%20webhook_url%2C%0A%20%20%20%20%20%20%20%20treatment_group%20%7B%0A%20%20%20%20%20%20%20%20%20%20__typename%0A%20%20%20%20%20%20%20%20%20%20...%20on%20Treatment_group%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20treatment_group%2C%0A%20%20%20%20%20%20%20%20%20%20%20%20reporting_group%2C%0A%20%20%20%20%20%20%20%20%20%20%20%20record_type_id%0A%20%20%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20contact_data_step%20%7B%0A%20%20%20%20%20%20%20%20%20%20__typename%20%0A%20%20%20%20%20%20%20%20%20%20...%20on%20Contact_data_step%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20contact_data_step_name%2C%0A%20%20%20%20%20%20%20%20%20%20%20%20contact_data_step_label%2C%0A%20%20%20%20%20%20%20%20%20%20%20%20firstname_label%2C%0A%20%20%20%20%20%20%20%20%20%20%20%20firstname_error%2C%0A%20%20%20%20%20%20%20%20%20%20%20%20lastname_label%2C%0A%20%20%20%20%20%20%20%20%20%20%20%20lastname_error%2C%0A%20%20%20%20%20%20%20%20%20%20%20%20email_label%2C%0A%20%20%20%20%20%20%20%20%20%20%20%20email_error%2C%0A%20%20%20%20%20%20%20%20%20%20%20%20phone_label%2C%0A%20%20%20%20%20%20%20%20%20%20%20%20phone_error%2C%0A%20%20%20%20%20%20%20%20%20%20%20%20legal_text%2C%0A%20%20%20%20%20%20%20%20%20%20%20%20button_label%0A%20%20%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20%7D%2C%0A%20%20%20%20%20%20%20thank_you_url%20%7B%0A%20%20%20%20%20%20%20%20%20%20__typename%0A%20%20%20%20%20%20%20%20%20%20...%20on%20_ExternalLink%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20url%0A%20%20%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20questions%20%7B%0A%20%20%20%20%20%20%20%20%20%20question%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20__typename%20%0A%20%20%20%20%20%20%20%20%20%20%20%20...%20on%20Question%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20question_label%2C%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20question_explanation%2C%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20salesforce_field%2C%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20options%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20option_label%2C%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20option_value%2C%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20option_icon%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%7D%0A%20%20%20%20%7D%0A%20%20%7D%0A%7D%0A`,
                headers: {
                  Authorization:
                    "Token MC5YZDUwSHhFQ UFBTkFaTVJ4.X27vv71M77-977-977-977-977-9KO-_ve-_vXFhY2Lvv73vv71w77-977-9Tu-_ve-_ve-_ve-_ve-_vQfvv73vv71Q77-9",
                  "Prismic-Ref": ref
                }
              };
              return config;
            };

            // fetch and store data to state
            axios(getConfig())
              .then(response => {
                this.state.data = response.data;
              })
              .catch(error => {
                this.state.errors.push(error)
              });
          })
          .catch(error => {
            this.state.errors.push(error)
          });
      } else {
        this.state.errors.push(error)
      }
    },

    sendData({ commit }, data) {
      // send collected data to webhook

      axios({
        method: "post",
        url: this.state.data.data.allFunnels.edges[0].node.webhook_url,
        data: data
      }).then(response => {
        if (response.status != 200) {
          window.top.location.href = data.next_page;
          return;
        }
        let next = response.data.next_page;
        window.top.location.href = `${next[0]}://${next[1]}${next[2]}?${next[4]}`;
      });
    },

    sendTrackingEvent({ commit}, stepName) {
      
      // prepare data that we need to contruct events
      const node = this.state.data.data.allFunnels.edges[0].node;
      const params = this.state.params;

      const evtProp = {
        'Funnel': node.funnel_id,
        'Funnel Variant': `${node.funnel_id} - Variant ${params.variant}`,
        'Page': params.page,
        'Page Variant': `${params.page} - Variant ${params.variant == undefined ? 'Unknown' : params.variant}`,
        'Step': stepName,
        'Previous Step': this.state.step - 1 == 0 ? '-' : this.state.step - 1,
        'Action': 'Page View',
        'Action Step': `Page View - ${params.step}`,
        'Action Funnel': `Page View - ${node.funnel_id}`,
        'Action Category': `Page View - ${params.page}`,
        'User IP': this.state.ip,
        'PAV IP': this.state.ip ===  null ? 'Unkwnown' : params.isPavAccess ? 'Yes' : 'No',
        'Error Message': this.state.errors.length > 0 ? this.state.errors.join(' AND ') : 'No errors detected'
      };

      Mixpanel.track(stepName, evtProp);
    }
  }
});
  
export default store;
