import Vue from 'vue';
import Vuex from 'vuex';
import VueRouter from 'vue-router';

import MultiVue from "vue-multivue";
import router from './routing/main';
import store from './store/index';

// Mixpanel
import Mixpanel from 'mixpanel-browser';
Mixpanel.init('700a5f9679f82a6afdb5b4b9c695d52e');

// Facebook
import VueFacebookPixel from 'vue-analytics-facebook-pixel';
Vue.use(VueFacebookPixel);
Vue.analytics.fbq.init('1707862066157334');

// components
import QunoFunnels from './components/QunoFunnels';

Vue.config.productionTip = false;
Vue.use(Vuex);
Vue.use(VueRouter);
Vue.use(Mixpanel);

new MultiVue(".vue-app-qunofunnels", {
  store,
  router: router,
  components: {
    'qunofunnels': QunoFunnels
  }
});